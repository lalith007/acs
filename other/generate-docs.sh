#!/usr/bin/env bash

modules=$2

title(){
  local name=$1
}

echo > docs.md

echo "Generating docs for AWS Account Config"
printf "# AWS Account Config\n\n" >> docs.md
terraform-docs md . >> docs.md

for m in $modules; do
  if [[ "$m" != "iam-role" ]]; then
    echo "generating docs for $m"
    printf "# $m\n\n" >> docs.md
    terraform-docs md $m >> docs.md
  fi
done
