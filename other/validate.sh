#!/usr/bin/env bash

modules=$(find -mindepth 2 -name *.tf -printf '%P\n' | xargs -I % dirname %)

(terraform validate ./src && echo "√ meta-module") || exit 1

for m in $modules; do
  (terraform validate $m && echo "√ $m validation") || exit 1
  (terraform fmt $m && echo "√ $m style-guide") || exit 1
#  (terraform get -update $m && echo "√ $m dependencies") || exit 1
done
