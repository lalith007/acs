
SRC = $(wildcard ./src/*.tf ./src/*/*.tf ./src/user-policies/*/*.tf)

modules = $(shell ls -1 $(SRC) | xargs -I % dirname %)

all:
	echo $(SRC)
	echo $(modules)


validate:
	@bash other/validate.sh

docs.md: $(SRC) $(modules)
	@bash other/generate-docs.sh

clean:
	-find . -type f -name '*tfplan*' -delete
	-find . -type f -name '*tfstate*' -delete
	-find . -name '.terraform' -exec rm -fr {} \;


.PHONY: validate clean
