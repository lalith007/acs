resource "aws_config_config_rule" "r" {
  name        = "${var.name}"
  description = "${var.description}"

  source {
    owner             = "${var.owner}"
    source_identifier = "${var.source_identifier}"
  }
}
