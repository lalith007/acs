variable "owner" {
  description = "AWS for AWS Managed Config Rules. CUSTOM_LAMBDA for customer managed Config Rules"
  default     = "AWS"
}

variable "source_identifier" {
  description = "For owner = CUSTOM_LAMBDA use the Lambda.arn. For owner = AWS see http://docs.aws.amazon.com/config/latest/developerguide/evaluate-config_use-managed-rules.html"
  default     = "CLOUD_TRAIL_ENABLED"
}

variable "name" {
  description = "Config Rule name that will be displayed in AWS Console"
  default     = "CLOUD_TRAIL_ENABLED"
}

variable "description" {
  description = "description of the Config Rule"
}
