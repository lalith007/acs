/**
* Config Rules setup
*/

/* Config rules to be implemented
*	module "root-mfa-enabled" {
*	  source = "root-mfa-enabled.tf"
*	  name = "RootMFAEnabled"
*	}
*
*	module "password-policy-exists" {
*	  source = "password-policy-exists.tf"
*	  name = "PasswordPolicyExists"
*	}
*
*/

//Supplies current AWS account ID
data "aws_caller_identity" "current" {}

//Required modules to enable Config Rules in all accounts
module "aws_config_configuration_recorder" {
  source     = "./aws-config-configuration-recorder"
  account_id = "${data.aws_caller_identity.current.account_id}"
}

/*
 * AWS Managed Config Rules that do not require no parameters
 */
module "cloud_trail_audit" {
  name              = "cloudtrail-enabled"
  description       = "Checks whether AWS CloudTrail is enabled in your AWS account. Optionally, you can specify which S3 bucket, SNS topic, and Amazon CloudWatch Logs ARN to use"
  source            = "./aws-managed-config-rules"
  owner             = "AWS"
  source_identifier = "CLOUD_TRAIL_ENABLED"
}

module "root_account_mfa_audit" {
  name              = "root-account-mfa-enabled"
  description       = "Checks whether users of your AWS account require a multi-factor authentication (MFA) device to sign in with root credentials"
  source            = "./aws-managed-config-rules"
  owner             = "AWS"
  source_identifier = "ROOT_ACCOUNT_MFA_ENABLED"
}

module "unrestricted_incoming_ssh_disabled" {
  name              = "unrestricted-incoming-ssh-disabled"
  description       = "Checks whether security groups that are in use disallow unrestricted incoming SSH traffic"
  source            = "./aws-managed-config-rules"
  owner             = "AWS"
  source_identifier = "INCOMING_SSH_DISABLED"
}

module "iam_user_no_attached_policies_check" {
  name              = "iam-user-no-attached-policies-check"
  description       = "Checks that none of your IAM users have policies attached. IAM users must inherit permissions from IAM groups or roles"
  source            = "./aws-managed-config-rules"
  owner             = "AWS"
  source_identifier = "IAM_USER_NO_POLICIES_CHECK"
}

module "eip_attached" {
  name              = "elastic-ip-attached"
  description       = "Checks whether all Elastic IP addresses that are allocated to a VPC are attached to EC2 instances or in-use elastic network interfaces (ENIs)"
  source            = "./aws-managed-config-rules"
  owner             = "AWS"
  source_identifier = "EIP_ATTACHED"
}

/*
 * AWS Managed Config Rules that do require additional parameters (input_parameters not available via terraform as of v 0.9.5 on AWS Managed Config Rules)
 */

module "iam_password_policy_audit" {
  name              = "iam-password-policy-enabled"
  description       = "Checks whether the account password policy for IAM users meets the specified requirements"
  source            = "./aws-managed-config-rules"
  owner             = "AWS"
  source_identifier = "IAM_PASSWORD_POLICY"
}

/*
 * Custom Config Rules
 */

