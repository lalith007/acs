/**
 * Creates an IAM Groups with an in-line policy to allow group members to assume
 * provided role. The group name will be prefixed from the map's key and the
 * value will be used in the groups' assume role policy.
 *
 * Usage:
 *
 *    variable "accounts" {
 *      type = "map"
 *
 *      default = {
 *        test-account-1 = "111111111111"
 *        test-account-2 = "222222222222"
 *      }
 *    }
 *
 *    module "developers_group" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-groups
 *      name     = "developers"
 *      path     = "/humans/"
 *      accounts = "${var.accounts}"
 *      role_arn = "arn:aws:iam::$${account_id}:role/administrator"
 *    }
 *
 */

resource "aws_iam_group" "default" {
  name  = "${join("-", list(element(keys(var.accounts), count.index), var.name))}"
  path  = "${var.path}"
  count = "${length(var.accounts)}"
}

resource "aws_iam_group_policy" "default" {
  name   = "${join("-", list("assume", element(keys(var.accounts), count.index), var.name, "role"))}"
  group  = "${element(aws_iam_group.default.*.id, count.index)}"
  policy = "${element(data.template_file.assume_role.*.rendered, count.index)}"
  count  = "${length(var.accounts)}"
}

data "template_file" "assume_role" {
  template = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Resource": "$${role_arn}"
  }
}
EOF

  vars {
    role_arn = "${element(data.template_file.role_arn.*.rendered, count.index)}"
  }

  count = "${length(var.accounts)}"
}

data "template_file" "role_arn" {
  template = "${var.role_arn}"

  vars {
    account_id = "${element(values(var.accounts), count.index)}"
  }

  count = "${length(var.accounts)}"
}
