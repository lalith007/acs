variable "name" {
  description = "The name of the group."
}

variable "path" {
  description = "Path in which to create the group."
  default     = "/"
}

variable "role_arn" {
  description = "The IAM Role's ARN to allow group members to assume, such as arn:aws:iam::*:role/administrator for admin groups."
}

variable "accounts" {
  description = "The number of groups to create, only 0 and 1 are currently supported."
  type        = "map"
}

// The group's ID.
output "ids" {
  value = ["${aws_iam_group.default.*.id}"]
}

// The ARN assigned by AWS for this group."
output "arns" {
  value = ["${aws_iam_group.default.*.arn}"]
}

// The group's name.
output "names" {
  value = ["${aws_iam_group.default.*.name}"]
}

// The path of the group in IAM.
output "paths" {
  value = ["${aws_iam_group.default.*.path}"]
}
