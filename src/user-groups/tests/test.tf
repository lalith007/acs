variable "accounts-list" {
  type = "map"

  default = {
    test-account-1 = "111111111111"
    test-account-2 = "222222222222"
  }
}

module "multi_groups" {
  source   = "../"
  name     = "multi"
  path     = "/test/"
  accounts = "${var.accounts-list}"
  role_arn = "arn:aws:iam::$${account_id}:role/multi"
}
