resource "aws_iam_role_policy" "default" {
  name = "${var.name_prefix}${var.name}${var.name_suffix}Policy"
  role = "${aws_iam_role.default.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "application-autoscaling:*",
        "cloudwatch:DescribeAlarms",
        "cloudwatch:PutMetricAlarm",
        "ecs:DescribeServices",
        "ecs:UpdateService"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  count = "${var.enable ? 1 : 0}"
}

resource "aws_iam_role" "default" {
  name = "${var.name_prefix}${var.name}${var.name_suffix}"
  path = "${var.path}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "application-autoscaling.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  count = "${var.enable ? 1 : 0}"
}
