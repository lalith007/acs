variable "name" {
  description = "Name of the policy's reasouces"
  default     = "AutoScalingService"
}

variable "name_prefix" {
  description = "Name prefix of reasources."
  default     = ""
}

variable "name_suffix" {
  description = "Name suffix of reasources."
  default     = ""
}

variable "path" {
  description = "Path in which to create the policy."
  default     = "/"
}

// https://github.com/hashicorp/terraform/issues/10857
variable "enable" {
  description = "Temp workaround requiring explicit setting to false to disable module."
  default     = true
}

// The ARN assigned by AWS to this role.
output "arn" {
  value = "${aws_iam_role.default.arn}"
}

// The path of the policy in IAM.
output "path" {
  value = "${aws_iam_role.default.path}"
}
