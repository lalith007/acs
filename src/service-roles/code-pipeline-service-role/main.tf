resource "aws_iam_role_policy" "default" {
  name = "${var.name_prefix}${var.name}${var.name_suffix}Policy"
  role = "${aws_iam_role.default.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::codepipeline*",
        "arn:aws:s3:::elasticbeanstalk*"
      ]
    },
    {
      "Action": [
        "cloudformation:*",
        "cloudwatch:*",
        "codebuild:BatchGet*",
        "codebuild:BatchGetBuilds",
        "codebuild:Get*",
        "codebuild:List*",
        "codebuild:StartBuild",
        "codebuild:StopBuild",
        "codecommit:CancelUploadArchive",
        "codecommit:GetBranch",
        "codecommit:GetCommit",
        "codecommit:GetRepository",
        "codecommit:GetUploadArchiveStatus",
        "codecommit:ListBranches",
        "codecommit:UploadArchive",
        "codedeploy:CreateDeployment",
        "codedeploy:GetApplicationRevision",
        "codedeploy:GetDeployment",
        "codedeploy:GetDeploymentConfig",
        "codedeploy:RegisterApplicationRevision",
        "events:*",
        "events:PutRule",
        "events:RemoveTargets",
        "iam:CreateRole",
        "iam:DeleteRole",
        "iam:DeleteRolePolicy",
        "iam:GetInstanceProfile",
        "iam:GetRole",
        "iam:ListInstanceProfiles",
        "iam:PassRole",
        "lambda:AddPermission",
        "lambda:CreateFunction",
        "lambda:DeleteFunction",
        "lambda:GetFunctionConfiguration",
        "lambda:InvokeFunction",
        "lambda:ListFunctions",
        "lambda:RemovePermission",
        "lambda:UpdateFunctionCode",
        "lambda:UpdateFunctionConfiguration",
        "logs:*",
        "s3:CreateBucket",
        "s3:GetBucketLocation",
        "s3:GetBucketVersioning",
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:List*",
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  count = "${var.enable ? 1 : 0}"
}

resource "aws_iam_role" "default" {
  name = "${var.name_prefix}${var.name}${var.name_suffix}"
  path = "${var.path}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "codepipeline.amazonaws.com",
          "cloudformation.amazonaws.com"
        ]
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  count = "${var.enable ? 1 : 0}"
}
