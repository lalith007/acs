variable "name_prefix" {
  description = "Name prefix of reasources."
  default     = ""
}

variable "name_suffix" {
  description = "Name suffix of reasources."
  default     = ""
}

variable "path" {
  description = "Path in which to create the policy."
  default     = "/"
}

// https://github.com/hashicorp/terraform/issues/10857
variable "enable" {
  description = "Temp workaround requiring explicit setting to false to disable module."
  default     = true
}

variable "service_roles" {
  description = "List of default ServiceRoles"

  default = [
    "CloudFormationServiceRole",
    "CodePipelineServiceRole",
    "CodebuildServiceRole",
    "LambdaExecuteCodePipelineEventServiceRole",
    "AutoScalingServiceRole",
    "EcsInstanceProfileServiceRole",
    "EcsDynamoTaskServiceRole",
  ]
}
