/**
 * Creates IAM ServiceRoles by name.
 * ServiceRoles allow internal AWS Services to assume Roles,
 * giving the Service the access rights required to perform API calls
 * within AWS.
 *
 * ServiceRole options are:
 *  "CloudFormationServiceRole",
 *  "CodePipelineServiceRole",
 *  "CodebuildServiceRole",
 *  "LambdaExecuteCodePipelineEventServiceRole",
 *  "AutoScalingServiceRole",
 *  "EcsInstanceProfileServiceRole",
 *  "EcsDynamoTaskServiceRole"
 *
 * Usage:
 *
 *    module "service-roles" {
 *      source           = "git::https://gitlab.com/aws-account-tools/acs.git/src/service-roles"
 *      service_roles    = [
 *        "CloudFormationServiceRole",
 *        "CodePipelineServiceRole",
 *        "CodebuildServiceRole",
 *        "LambdaExecuteCodePipelineEventServiceRole",
 *        "AutoScalingServiceRole",
 *        "EcsInstanceProfileServiceRole",
 *        "EcsDynamoTaskServiceRole"
 *      ]
 *    }
 *
 */

module "auto_scaling_service_role" {
  source      = "./auto-scaling-service-role"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.service_roles),"/.*AutoScalingServiceRole.*/", "1") == "1"  ? true : false}"
}

module "cloud_formation_service_role" {
  source      = "./cloud-formation-service-role"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.service_roles),"/.*CloudFormationServiceRole.*/", "1") == "1" ? true : false}"
}

module "codebuild_service_role" {
  source      = "./code-build-service-role"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.service_roles),"/.*CodebuildServiceRole.*/", "1") == "1" ? true : false}"
}

module "codepipeline_service_role" {
  source      = "./code-pipeline-service-role"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.service_roles),"/.*CodePipelineServiceRole.*/", "1") == "1"  ? true : false}"
}

module "ecs_dynamo_task_service_role" {
  source      = "./ecs-dynamo-task-service-role"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.service_roles),"/.*EcsDynamoTaskServiceRole.*/", "1") == "1"  ? true : false}"
}

module "ecs_instance_profile_service_role" {
  source      = "./ecs-instance-profile-service-role"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.service_roles),"/.*EcsInstanceProfileServiceRole.*/", "1") == "1"  ? true : false}"
}

module "lambda_execute_codepipeline_event_service_role" {
  source      = "./lambda-execute-codepipeline-event-service-role"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.service_roles),"/.*LambdaExecuteCodePipelineEventServiceRole.*/", "1") == "1"  ? true : false}"
}
