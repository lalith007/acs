module "service_roles" {
  source      = "../"
  name_prefix = "test2-"

  service_roles = [
    "CloudFormationServiceRole",
    "CodePipelineServiceRole",
    "CodebuildServiceRole",
    "LambdaExecuteCodePipelineEventServiceRole",
    "AutoScalingServiceRole",
    "EcsInstanceProfileServiceRole",
    "EcsDynamoTaskServiceRole",
  ]
}
