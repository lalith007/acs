resource "aws_iam_role_policy" "default" {
  name = "${var.name_prefix}${var.name}${var.name_suffix}Policy"
  role = "${aws_iam_role.default.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "autoscaling:Describe*",
        "cloudformation:Describe*",
        "cloudformation:CreateChangeSet*",
        "cloudformation:UpdateStack",
        "cloudformation:List*",
        "cloudtrail:Describe*",
        "cloudtrail:Get*",
        "cloudwatch:Describe*",
        "cloudwatch:putMetricAlarm",
        "s3:List*",
        "s3:Get*",
        "s3:PutObject",
        "sns:Get*",
        "sns:List*",
        "sqs:Get*",
        "sqs:List*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  count = "${var.enable ? 1 : 0}"
}

resource "aws_iam_role" "default" {
  name = "${var.name_prefix}${var.name}${var.name_suffix}"
  path = "${var.path}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "cloudformation.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

  count = "${var.enable ? 1 : 0}"
}
