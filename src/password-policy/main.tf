/**
 * Provides chosen password policy on AWS Accounts.
 *
 * Note: Complicated passwords are not more secure, just more complicated. They
 * often get stored and written down else where and thus defeating the MFA
 * knowledge factor.
 *
 * Usage:
 *
 *    module "password_policy" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/password-policy"
 *      type    = "sane"
 *    }
 *
 */

resource "aws_iam_account_password_policy" "pci" {
  count                          = "${var.type == "pci" ? 1 : 0}"
  minimum_password_length        = 16
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  max_password_age               = 90
  password_reuse_prevention      = 4
  allow_users_to_change_password = true
  hard_expiry                    = false
}

resource "aws_iam_account_password_policy" "sane" {
  count                          = "${var.type == "sane" ? 1 : 0}"
  minimum_password_length        = 16
  require_lowercase_characters   = true
  require_numbers                = false
  require_uppercase_characters   = false
  require_symbols                = false
  max_password_age               = 180
  password_reuse_prevention      = 10
  allow_users_to_change_password = true
  hard_expiry                    = false
}
