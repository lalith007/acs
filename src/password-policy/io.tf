variable "type" {
  description = "The type of password policy to apply."
}

// The type of the applied password policy
output "type" {
  value = "${var.type}"
}
