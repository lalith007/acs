module "this" {
  source       = "../"
  name         = "this-test"
  path         = "/test/"
  policy_count = "2"

  policies_to_attach = [
    "arn:aws:iam::aws:policy/CloudWatchEventsFullAccess",
    "arn:aws:iam::aws:policy/AmazonSNSFullAccess",
  ]
}
