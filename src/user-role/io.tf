variable "name" {
  description = "Name of the the IAM role."
}

variable "trust_account_id" {
  description = "The account ID of the trusted AWS account."
  default     = ""
}

variable "saml_idp_arn" {
  description = "The ARN of the SAML identity provider."
  default     = ""
}

variable "path" {
  description = "Path in which to create the role."
  default     = "/"
}

variable "policies_to_attach" {
  description = "List of ARNs for policies you wish to attach to the role"
  default     = []
}

variable "policy_count" {
  description = "The number of policies you are attaching to the resource"
  default     = "0"
}

// The Amazon Resource Name (ARN) specifying the role.
output "arn" {
  value = "${aws_iam_role.default.arn}"
}

// The creation date of the IAM role.
output "create_date" {
  value = "${aws_iam_role.default.create_date}"
}

// The stable and unique string identifying the role.
output "unique_id" {
  value = "${aws_iam_role.default.unique_id}"
}

// The name of the role.
output "name" {
  value = "${var.name}"
}
