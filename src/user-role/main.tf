/**
 * Creates an IAM role for users with a trust policy allowing assumption from
 * a trusted AWS Account (including 'this' account) or a federated identity
 * provider.
 *
 * Usage:
 *
 *    module "developer" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-role"
 *      name     = "developer"
 *      path     = "/humans/"
 *      trust_account_id = "${var.trust_account_id}"
 *      policies_to_attach = ["somePolicyArn", "someOtherPolicyArn"]
 *      policy_count = 2
 *    }
 *
 */

resource "aws_iam_role" "default" {
  name = "${var.name}"
  path = "${var.path}"

  assume_role_policy = "${element(concat(
    data.template_file.trust_account.*.rendered,
    data.template_file.saml_idp.*.rendered,
    data.template_file.this_account.*.rendered
    ), 0)}"
}

resource "aws_iam_role_policy_attachment" "attached_policies" {
  count      = "${var.policy_count}"
  role       = "${aws_iam_role.default.name}"
  policy_arn = "${element(var.policies_to_attach, count.index)}"
}

data "template_file" "trust_account" {
  template = "${file("${path.module}/trust-account-policy.json")}"

  count = "${var.trust_account_id != "" && var.saml_idp_arn == "" ? 1 : 0}"

  vars {
    account_id = "${var.trust_account_id}"
  }
}

data "template_file" "saml_idp" {
  template = "${file("${path.module}/saml-idp-policy.json")}"

  count = "${var.trust_account_id == "" && var.saml_idp_arn != "" ? 1 : 0}"

  vars {
    idp_arn = "${var.saml_idp_arn}"
  }
}

data "aws_caller_identity" "current" {}

data "template_file" "this_account" {
  template = "${file("${path.module}/trust-account-policy.json")}"

  count = "${var.trust_account_id == "" && var.saml_idp_arn == "" ? 1 : 0}"

  vars {
    account_id = "${data.aws_caller_identity.current.account_id}"
  }
}
