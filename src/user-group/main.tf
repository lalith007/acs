/**
 * Creates an IAM Group and an in-line policy to allow group members to assume
 * provided role.
 *
 * Usage:
 *
 *    module "administrator_group" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-group
 *      name     = "administrators"
 *      path     = "/humans/"
 *      role_arn = "arn:aws:iam::*:role/administrator"
 *    }
 *
 */

resource "aws_iam_group" "default" {
  name = "${var.name}"
  path = "${var.path}"
}

resource "aws_iam_group_policy" "default" {
  name   = "assume_${var.name}_role_policy"
  group  = "${aws_iam_group.default.id}"
  policy = "${data.template_file.assume_role.rendered}"
}

data "template_file" "assume_role" {
  template = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Resource": "$${role_arn}"
  }
}
EOF

  vars {
    role_arn = "${var.role_arn}"
  }
}
