variable "name" {
  description = "The name of the group."
}

variable "path" {
  description = "Path in which to create the group."
  default     = "/"
}

variable "role_arn" {
  description = "The IAM Role's ARN to allow group members to assume, such as arn:aws:iam::*:role/administrator for admin groups."
}

// The group's ID.
output "id" {
  value = "${aws_iam_group.default.id}"
}

// The ARN assigned by AWS for this group."
output "arn" {
  value = "${aws_iam_group.default.arn}"
}

// The group's name.
output "name" {
  value = "${aws_iam_group.default.name}"
}

// The path of the group in IAM.
output "path" {
  value = "${aws_iam_group.default.path}"
}

// The unique ID assigned by AWS.
output "unique_id" {
  value = "${aws_iam_group.default.unique_id}"
}
