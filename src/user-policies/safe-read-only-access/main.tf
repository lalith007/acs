/**
 * Creates an IAM policy for a Read Only role but unlike the AWS managed
 * policy this one is without privileges to data store contents, hence "safe".
 *
 * Usage:
 *
 *    module "safe_read_only" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/safe-read-only"
 *    }
 *
 */

resource "aws_iam_policy" "default" {
  name        = "${var.name_prefix}${var.name}${var.name_suffix}"
  path        = "${var.path}"
  description = "Safer read-only policy that limits access to data."
  policy      = "${data.template_file.iam_policy.rendered}"
  count       = "${var.enable ? 1 : 0}"
}

data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"
  count    = "${var.enable ? 1 : 0}"
}
