module "role_policies" {
  source      = "../"
  name_prefix = "test2-"

  policies = [
    "SafeReadOnlyAccess",
    "SafePowerUserAccess",
    "AdministratorAccess",
    "PowerUserAccess",
    "DevOpsAccessAccess",
    "CreateGroupsUsersDeny",
  ]
}

/*resource "aws_iam_group" "test_group" {
  name = "test-group"
  path = "/tests/"
}

module "group_policy" {
  source      = "../"
  name_prefix = "test2-"

  policies = [
    "IAMSelfServe",
  ]

  groups = [
    "resource.aws_iam_group.test_group.name",
  ]
}*/

