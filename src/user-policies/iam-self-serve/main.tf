/**
 * Creates an IAM policy that allows users to manage their own IAM resources
 * such as AwS MFA and passwords. Intended to be applied to groups in a trusted
 * AWS account and not to users or roles.
 *
 * Usage:
 *
 *    module "user_iam_self_serve" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/user-iam-self-serve"
 *    }
 *
 */

data "aws_caller_identity" "current" {}

resource "aws_iam_policy" "default" {
  name        = "${var.name_prefix}${var.name}${var.name_suffix}"
  path        = "${var.path}"
  description = "Allows IAM users to admin their own user, i.e. credentials and MFA."
  policy      = "${data.template_file.iam_policy.rendered}"
  count       = "${var.enable ? 1 : 0}"
}

data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"

  vars {
    account_id = "${data.aws_caller_identity.current.account_id}"
  }

  count = "${var.enable ? 1 : 0}"
}
