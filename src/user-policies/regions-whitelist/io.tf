variable "name" {
  description = "Name of the policy's reasouces"
  default     = "RegionsWhitelist"
}

variable "name_prefix" {
  description = "Name prefix of reasources."
  default     = ""
}

variable "name_suffix" {
  description = "Name suffix of reasources."
  default     = ""
}

variable "path" {
  description = "Path in which to create the policy."
  default     = "/"
}

variable "regions" {
  description = "Regions to allow EC2 API actions."

  default = [
    "us-west-2",
    "us-east-1",
  ]
}

// https://github.com/hashicorp/terraform/issues/10857
variable "enable" {
  description = "Temp workaround requiring explicit setting to false to disable module."
  default     = true
}

// The policy's ID.
output "id" {
  value = "${aws_iam_policy.default.id}"
}

// The ARN assigned by AWS to this policy.
output "arn" {
  value = "${aws_iam_policy.default.arn}"
}

// The description of the policy.
output "description" {
  value = "${aws_iam_policy.default.description}"
}

// The name of the policy.
output "name" {
  value = "${aws_iam_policy.default.name}"
}

// The path of the policy in IAM.
output "path" {
  value = "${aws_iam_policy.default.path}"
}

// The policy document.
output "policy" {
  value = "${aws_iam_policy.default.policy}"
}
