/**
 * Creates an IAM policy that denies EC2 API access to non-whitelisted
 * regions from roles.
 *
 * Usage:
 *
 *    module "ec2_regions_whitelist" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/regions-whitelist"
 *    }
 *
 */

resource "aws_iam_policy" "default" {
  name        = "${var.name_prefix}${var.name}${var.name_suffix}"
  path        = "${var.path}"
  description = "Limits the EC2 API to a whitelist of EC2 types."
  policy      = "${data.template_file.iam_policy.rendered}"
  count       = "${var.enable ? 1 : 0}"
}

data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"

  vars {
    regions = "${join("\", \"", var.regions)}"
  }

  count = "${var.enable ? 1 : 0}"
}
