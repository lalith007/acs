module "policy" {
  source = "../"
  name   = "test-policy"

  regions = [
    "us-west-2",
    "us-east-1",
  ]
}

module "test_role1" {
  source       = "../../../user-role"
  name         = "test-role1"
  path         = "/tests/"
  policy_count = "1"

  policies_to_attach = [
    "${module.policy.arn}",
  ]
}
