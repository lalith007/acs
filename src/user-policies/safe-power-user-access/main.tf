/**
 * Creates an IAM policy for a Power User role but unlike the AWS managed
 * policy this one is without privileges to security serives, hence "safe".
 *
 * Usage:
 *
 *    module "devops_user" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/safe-power-user"
 *    }
 *
 */

resource "aws_iam_policy" "default" {
  name        = "${var.name_prefix}${var.name}${var.name_suffix}"
  path        = "${var.path}"
  description = "Allows users to administer most services but not security services."
  policy      = "${data.template_file.iam_policy.rendered}"
  count       = "${var.enable ? 1 : 0}"
}

data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"
  count    = "${var.enable ? 1 : 0}"

  vars {
    account_id                 = "${data.aws_caller_identity.current.account_id}"
    allowed_service_roles_path = "${var.allowed_service_roles_path}"
  }
}

data "aws_caller_identity" "current" {}
