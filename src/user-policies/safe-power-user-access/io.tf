variable "name" {
  description = "Name of the policy's reasouces"
  default     = "SafePowerUserAccess"
}

variable "name_prefix" {
  description = "Name prefix of reasources."
  default     = ""
}

variable "name_suffix" {
  description = "Name suffix of reasources."
  default     = ""
}

variable "path" {
  description = "Path in which to create the policy."
  default     = "/"
}

// https://github.com/hashicorp/terraform/issues/10857
variable "enable" {
  description = "Temp workaround requiring explicit setting to false to disable module."
  default     = true
}

variable "allowed_service_roles_path" {
  description = "IAM Path of allowed ServiceRoles - used to limit iam:PassRole ie. arn:aws:iam::${account_id}:role/${allowed_service_roles_path}/*"
  default     = "/service-roles/"
}

// The policy's ID.
output "id" {
  value = "${aws_iam_policy.default.id}"
}

// The ARN assigned by AWS to this policy.
output "arn" {
  value = "${aws_iam_policy.default.arn}"
}

// The description of the policy.
output "description" {
  value = "${aws_iam_policy.default.description}"
}

// The name of the policy.
output "name" {
  value = "${aws_iam_policy.default.name}"
}

// The path of the policy in IAM.
output "path" {
  value = "${aws_iam_policy.default.path}"
}

// The policy document.
output "policy" {
  value = "${aws_iam_policy.default.policy}"
}
