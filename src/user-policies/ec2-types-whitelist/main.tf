/**
 * Creates an IAM policy that denies EC2 API access for non-whitelisted EC2
 * types from roles.
 *
 * Usage:
 *
 *    module "ec2_types_whitelist" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/ec2-types-whitelist"
 *
 *       types = [
 *         "t2.*",
 *         "m4.large",
 *         "m4.xlarge",
 *         "m4.2xlarge",
 *         "m4.4xlarge",
 *       ]
 *    }
 *
 */

resource "aws_iam_policy" "default" {
  name        = "${var.name_prefix}${var.name}${var.name_suffix}"
  path        = "${var.path}"
  description = "Limits the EC2 API to a whitelist of EC2 types."
  policy      = "${data.template_file.iam_policy.rendered}"
  count       = "${var.enable ? 1 : 0}"
}

data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"

  vars {
    types = "${join("\", \"", var.types)}"
  }

  count = "${var.enable ? 1 : 0}"
}
