module "ec2_types_whitelist" {
  source = "../"

  types = [
    "t2.*",
    "m4.large",
    "m4.xlarge",
    "m4.2xlarge",
    "m4.4xlarge",
  ]
}
