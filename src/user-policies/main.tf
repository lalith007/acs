/**
 * Creates IAM policies by name.
 *
 * Policy options are:
 *  - SafePowerUserAccess
 *  - DevOpsAccess
 *  - SafeReadOnlyAccess
 *  - IAMSelfServe
 *
 * Usage:
 *
 *    module "policies" {
 *      source      = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies"
 *      policies    = [
 *        "SafeReadOnlyAccess",
 *        "SafePowerUserAccess",
 *        "DevOpsAccess"
 *      ]
 *    }
 *
 */

module "devops_access" {
  source      = "./devops-access"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.policies),"/.*DevOpsAccess.*/", "1") == "1"  ? true : false}"
}

module "safe_power_user_access" {
  source      = "./safe-power-user-access"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.policies),"/.*SafePowerUserAccess.*/", "1") == "1" ? true : false}"
}

module "safe_read_only_access" {
  source      = "./safe-read-only-access"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.policies),"/.*SafeReadOnlyAccess.*/", "1") == "1" ? true : false}"
}

module "create_groups_users_deny" {
  source      = "./create-groups-users-deny"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.policies),"/.*CreateGroupsUsersDeny.*/", "1") == "1"  ? true : false}"
}

module "iam_self_serve" {
  source      = "./iam-self-serve"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.policies),"/.*IAMSelfServe.*/", "1") == "1" ? true : false}"
}

module "regions_whitelist" {
  source      = "./regions-whitelist"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.policies),"/.*RegionsWhitelist.*/", "1") == "1"  ? true : false}"
}

module "ec2_types_whitelist" {
  source      = "./ec2-types-whitelist"
  name_prefix = "${var.name_prefix}"
  name_suffix = "${var.name_suffix}"
  path        = "${var.path}"
  enable      = "${replace(join(" ", var.policies),"/.*EC2TypesWhitelist.*/", "1") == "1"  ? true : false}"
}
