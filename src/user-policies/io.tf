variable "name_prefix" {
  description = "Name prefix of reasources."
  default     = ""
}

variable "name_suffix" {
  description = "Name prefix of reasources."
  default     = ""
}

variable "path" {
  description = "Path in which to create the policies."
  default     = "/"
}

variable "policies" {
  description = "List of policies."

  default = [
    "SafeReadOnlyAccess",
    "SafePowerUserAccess",
    "DevOpsAccess",
  ]
}
