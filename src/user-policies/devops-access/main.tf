/**
 * Creates an IAM policy that provides DevOps actions suitable for prod
 * environments. This policy should be used in conjunction with a ReadOnly
 * policy. Together, they provide access set of safe actions for prod
 * environments without any create/delete privileges for resources.
 *
 * Usage:
 *
 *    module "devops" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/devops-user"
 *    }
 *
 */

resource "aws_iam_policy" "default" {
  name        = "${var.name_prefix}${var.name}${var.name_suffix}"
  path        = "${var.path}"
  description = "DevOps actions suitable for prod environments, without create/delete privileges."
  policy      = "${data.template_file.iam_policy.rendered}"
  count       = "${var.enable ? 1 : 0}"
}

data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"
  count    = "${var.enable ? 1 : 0}"
}
