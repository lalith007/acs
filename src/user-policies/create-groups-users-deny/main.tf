/**
 * Creates an IAM policy that denies creating users or groups.
 *
 * Usage:
 *
 *    module "create_group_users_deny" {
 *      source  = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/create-group-users-deny"
 *    }
 *
 */

resource "aws_iam_policy" "default" {
  name        = "${var.name_prefix}${var.name}${var.name_suffix}"
  path        = "${var.path}"
  description = "Denies the creation of IAM Groups and Users."
  policy      = "${data.template_file.iam_policy.rendered}"
  count       = "${var.enable ? 1 : 0}"
}

data "template_file" "iam_policy" {
  template = "${file("${path.module}/policy.json")}"
  count    = "${var.enable ? 1 : 0}"
}
