# AWS Account Config Stack

This set of Terraform modules provides IAM resources and configuration for an
AWS multi-account strategy. Convention is preferred over configuration, but the
set is completely modularized allowing direct consumption of submodules and
individualized configuration.

#### AWS Account Config Stack comes with:

- password policies
- administrator role and group with role assuming policies
- developer role and group with role assuming policies
- services group
- cross account federation
- ec2 white-lists
- MFA polices

## QuickStart

Suggest using a single private git repo with a subdirectory for each AWS
Account and a common directory for custom modules you create that should
not be shared with the world.

The non-trust accounts, that is an account that does not have IAM users can
consist of a single main.tf such as:

```js
provider "aws" {
  alias               = "${var.name}"
  region              = "${var.region}"
  allowed_account_ids = ["${lookup(module.aws_accounts_list.map, var.name)}"]
}

module "aws_accounts_list" {
  source = "../common/aws_accounts_list"
}

module "aws_account" {
  source           = "git::https://gitlab.com/aws-account-tools/acs.git//src"
  trust_account_id = "${var.trust_account_id}"
  name             = "${var.name}"
}

module "create_groups_users_deny" {
  source = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/create-groups-users-deny"

  roles = [
    "${module.aws_account.roles["developers"]["name"]}",
    "${module.aws_account.roles[administrators]["name"]}",
}

```

and the trust account could consist of:

```js
provider "aws" {
  alias               = "${var.name}"
  region              = "${var.region}"
  allowed_account_ids = ["${lookup(module.aws_accounts_list.map, var.name)}"]
}

module "aws_accounts_list" {
  source = "../common/aws_accounts_list"
}

module "aws_account" {
  source           = "git::https://gitlab.com/aws-account-tools/acs.git//src"
  trust_account_id = "${var.trust_account_id}"
  name             = "${var.name}"
  is_trust_account = true
}

module "ec2_regions_whitelist" {
  source = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/ec2-regions-whitelist"

  regions = [
    "us-west-2",
    "us-east-1",
  ]

  groups = [
    "${module.devops_developers.name}",
    "${module.stage_developers.name}",
    "${module.production_developers.name}",
  ]
}

module "ec2_types_whitelist" {
  source = "git::https://gitlab.com/aws-account-tools/acs.git//src/user-policies/ec2-types-whitelist"

  types = [
    "t2.*",
    "m4.large",
    "m4.xlarge",
  ]

  groups = [
    "${module.devops_developers.name}",
    "${module.stage_developers.name}",
    "${module.production_developers.name}",
  ]
}
```
